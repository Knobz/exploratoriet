import kivy
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import NumericProperty
from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.core.window import Window
from numpy import source
from barcode_scanner import Barcode_scanner
from kivy.clock import Clock
from kivy.lang import Builder
import random
kivy.require("2.0.0")
import os
import csv
import serial
import gc
import time
import glob

RESET = 0
IDLE = "999059" #Startläge
START = "999060" #Starta spelat – scanna varor (Sen kan ju spelet starta automatiskt också om någon börjar scanna varor direkt).
SHOW_RESULT = "999061" #Handlat klart (visa resultatet)

#PATH = "/home/pi/Documents/teknikens_hus/Food/ui/"
PATH = "C:\\Users\\admin\\Documents\\Python\\Exploratoriet\\"
#BG_IMAGE = PATH + "Grafik/Grafik-kassa-bgrd.png"
BG_IMAGE = PATH + "Grafik\\Bakgrundsbild_handla.png"
#ENVIROMENTAL = PATH + "Grafik/"
ENVIROMENTAL = PATH + "Grafik\\"
ENVIROMENTAL_NEGATIVE = "Knappa-ajdå val.png"
ENVIROMENTAL_NEUTRAL = "Knappa-nja val.png"
ENVIROMENTAL_POSITIVE = "Knappa-snyggt val.png"
PAYED_IMAGE_BAD = Image(source = PATH + "Grafik\\Bakgrundsbild_resultat_dåligt.png", size_hint=(1,1))
PAYED_IMAGE_NEUTRAL = Image(source = PATH + "Grafik\\Bakgrundsbild_resultat_mellan.png", size_hint=(1,1))
PAYED_IMAGE_GOOD = Image(source = PATH + "Grafik\\Bakgrundsbild_resultat_bra.png", size_hint=(1,1))

DEFAULT_COLOR = (0, 0, 0, 1)
ENVIROMENTAL_SCORE = 0

CART_LIMIT = [1540, 160, 1770, 240, 1320, 560, 1700, 700]
 
#Window.resize = False
Window.size = (1920, 1080)

Window.borderless = True
Window.fullscreen = "auto"
#Window.show_cursor = False
#Window.clear_widgets()
class MyLabel(Label):
   def on_size(self, *args):
      self.text_size = self.size

class ui(FloatLayout):
    def __init__(self, **kwargs):
        super(ui, self).__init__(**kwargs)
        self.background_image = Image(source=BG_IMAGE, size_hint=(1,1))
        self.add_widget(self.background_image)
        
        self.main_layout = BoxLayout(size_hint=(1,1), orientation="horizontal")
        self.add_widget(self.main_layout)
        self.left_side_box = BoxLayout(orientation="vertical", size_hint=(1,1))
        self.main_layout.add_widget(self.left_side_box)
        self.cart = FloatLayout()
        self.main_layout.add_widget(self.cart)
        
        self.indicator_box = BoxLayout(size_hint=(555/(1920/2), 183/1080), orientation="vertical")
        self.left_side_box.add_widget(self.indicator_box)
        self.goods_box = BoxLayout(size_hint=(1, 1), orientation="vertical")
        self.left_side_box.add_widget(self.goods_box)
        self.goods_framing_box = BoxLayout(orientation="horizontal")
        self.goods_box.add_widget(self.goods_framing_box)
        self.goods_left_framing = Label(size_hint=(300/(1920/2), 1))
        self.goods_framing_box.add_widget(self.goods_left_framing)
        self.goods = BoxLayout(orientation="vertical", padding=(10,155))
        self.goods_framing_box.add_widget(self.goods)
        
        """self.indicator_box_framing_label_top = Label(size_hint=(1, 14/197))
        self.indicator_box.add_widget(self.indicator_box_framing_label_top)
        self.indicator_box_side_framing_box = BoxLayout(orientation="horizontal", size_hint=(1,183/197))
        self.indicator_box.add_widget(self.indicator_box_side_framing_box)
        self.indicator_box_framing_label_left = Label(size_hint=(28/583, 1))
        self.indicator_box_side_framing_box.add_widget(self.indicator_box_framing_label_left)"""
        
        self.enviromental_affect_image = Image(source=ENVIROMENTAL + "Knappar - släckta.png", pos_hint={'right': 1, 'bottom': 1})
        self.indicator_box.add_widget(self.enviromental_affect_image)
        
        self.s0 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s1 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s2 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s3 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s4 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s5 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s6 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s7 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s8 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s9 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s10 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s11 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s12 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s13 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        
        
        self.goods.add_widget(self.s0)
        self.goods.add_widget(self.s1)
        self.goods.add_widget(self.s2)
        self.goods.add_widget(self.s3)
        self.goods.add_widget(self.s4)
        self.goods.add_widget(self.s5)
        self.goods.add_widget(self.s6)
        self.goods.add_widget(self.s7)
        self.goods.add_widget(self.s8)
        self.goods.add_widget(self.s9)
        self.goods.add_widget(self.s10)
        self.goods.add_widget(self.s11)
        self.goods.add_widget(self.s12)
        self.goods.add_widget(self.s13)
        
        
        self.s_list = [self.s0,self.s1,self.s2,self.s3,self.s4,self.s5,self.s6,self.s7,self.s8,self.s9,self.s10,self.s11,self.s12,self.s13]
        

class RotatedImage(Image):
    angle = NumericProperty()
        
class hello(App):
    def build(self):
        self.items_in_cart = []
        self.already_scanned = []
        
        Builder.load_string('''
<RotatedImage>:
    canvas.before:
        PushMatrix
        Rotate:
            angle: root.angle
            axis: 0, 0, 1
            origin: root.center
    canvas.after:
        PopMatrix
''')
        self.barcode = Barcode_scanner()
        Clock.schedule_interval(self.mainloop, 1 / 30.)
        self.new_gui = ui()
        return self.new_gui
    
    def show_info(self, info):
        try:
            self.product
            self.new_gui.remove_widget(self.information)
        except:
            pass
        self.information = Image(source=info)
        self.new_gui.add_widget(self.information)
        Clock.schedule_once(self.hide_info, 3)
        
    def show_product(self, prod):
        try:
            self.product
            self.new_gui.remove_widget(self.product)
        except:
            pass
        self.product = RotatedImage(source=prod, size_hint=(None, None), pos_hint={'center_x': 0.5, 'center_y': 0.5}, size=((500,500)), nocache=True)
        self.new_gui.add_widget(self.product)
        Clock.schedule_once(self.hide_product, 3)
        
    def hide_product(self,dt):
        self.new_gui.remove_widget(self.product)
        gc.collect()
        pass
    
    def hide_info(self,dt):
        self.new_gui.remove_widget(self.information)
        gc.collect()
    
    def add_image_to_cart(self,image, dt):
        self.new_gui.cart.add_widget(image)
        
    def mainloop(self, dt):
        global ENVIROMENTAL_SCORE
        if self.barcode.result:
            match = ""
            name = ""
            rows = []
            fields = []
            add_price = 0
            #C:\Users\admin\Documents\Python\Exploratoriet\exploratoriet\Food\ui\Taco.csv
            with open(PATH + "exploratoriet\\Food\\ui\\" + "Taco.csv", 'r', encoding="utf8") as csvfile:
                # creating a csv reader object
                csvreader = csv.reader(csvfile, delimiter=";")
                # extracting field names through first row
                fields = next(csvreader)
                # extracting each data row one by one
                for row in csvreader:
                    rows.append(row)
                for row in rows:
                    if str(self.barcode.result) == IDLE:
                        break
                    elif str(self.barcode.result) == START:
                        break
                    elif str(self.barcode.result) == SHOW_RESULT:
                        if self.items_in_cart > 4:
                            
                            if ENVIROMENTAL_SCORE > 17:
                                self.show_info(PAYED_IMAGE_GOOD)
                            elif ENVIROMENTAL_SCORE > -1:
                                self.show_info(PAYED_IMAGE_NEUTRAL)
                            elif ENVIROMENTAL_SCORE < 0:
                                self.show_info(PAYED_IMAGE_BAD)
                            time.sleep(5)
                            break
                    
                    if str(self.barcode.result) == str(row[1]).replace(" ", ""):
                        if glob.glob(PATH + "exploratoriet\\varor_kassa\\" + str(row[0]) + ".png"):
                            match = str(row[0]) + ".png"
                            name = str(row[0])
                            ENVIROMENTAL_SCORE = ENVIROMENTAL_SCORE + int(row[2])
                            self.current_score = int(row[2])
                        break
                if match:                                                             #CART_LIMIT = [1540, 160, 1770, 240, 1320, 560, 1700, 700]
                    if match not in self.already_scanned:
                        x = random.randint(800,1100)
                        y = random.randint(100,650)
                        item = RotatedImage(source=f"{PATH}\\exploratoriet\\Varor_kassa\\{match}", angle=random.randint(0,360), pos=(x,y), size_hint=(None, None), size=((250,250)), nocache=True)
                        Clock.schedule_once(lambda dt:self.new_gui.cart.add_widget(item), 3 )
                        if len(self.items_in_cart) > 30:
                            self.new_gui.cart.remove_widget(self.items_in_cart[0])
                            gc.collect()
                            self.items_in_cart = self.items_in_cart[1:]
                        self.items_in_cart.append(item)
                        self.show_product(f"{PATH}\\exploratoriet\\Varor_kassa\\{match}")
                        self.already_scanned.append(match)
                        if self.current_score < 0:
                            self.new_gui.enviromental_affect_image.source = ENVIROMENTAL + ENVIROMENTAL_NEGATIVE
                        elif self.current_score == 0:
                            self.new_gui.enviromental_affect_image.source = ENVIROMENTAL + ENVIROMENTAL_NEUTRAL
                        elif self.current_score > 1:
                            self.new_gui.enviromental_affect_image.source = ENVIROMENTAL + ENVIROMENTAL_POSITIVE
                        print(ENVIROMENTAL_SCORE)
                    
                else:
                    self.show_product(f"{PATH}\\exploratoriet\\Food\\ui\\Ingen_vara\\ingen_vara.png")

                
                
            if name:
                for i in range(14):
                    label = self.new_gui.s_list[i]
                    if label.text == name:
                        break
                else:
                    for i in range(14):
                        label = self.new_gui.s_list[i]
                        if label.text == "":
                            label.text = name
                            break
                for i in range(14):
                    if self.new_gui.s_list[i].text is not "":
                        pass
                    else:
                        break
                else:
                    self.show_info(PATH + "Grafik\\meddelande_du_har_handlat_max.png")
                               
                
            self.barcode.result = ""
hello().run()