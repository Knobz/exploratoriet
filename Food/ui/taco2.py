from cgitb import reset
from tkinter import font
import kivy
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import NumericProperty
from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.core.window import Window
from numpy import source
from barcode_scanner import Barcode_scanner
from kivy.clock import Clock
from kivy.lang import Builder
import random
kivy.require("2.0.0")
import os
import csv
import serial
import gc
import time
import glob

RESET = 0
START = "999059" #Startläge
SHOW_RESULT = "999061" #Handlat klart (visa resultatet)

PATH = "/home/expo/Documents/exploratoriet/"
#PATH = "C:\\Users\\admin\\Documents\\Python\\Exploratoriet\\"
BG_IMAGE = PATH + "Grafik/Bakgrundsbild_handla.png"
#BG_IMAGE = PATH + "Grafik\\Bakgrundsbild_handla.png"
ENVIROMENTAL = PATH + "Grafik/"
#ENVIROMENTAL = PATH + "Grafik\\"
ENVIROMENTAL_NEGATIVE = "Knappa-ajdå val.png"
ENVIROMENTAL_NEUTRAL = "Knappa-nja val.png"
ENVIROMENTAL_POSITIVE = "Knappa-snyggt val.png"
#PAYED_IMAGE_BAD = Image(source = PATH + "Grafik\\Bakgrundsbild_resultat_dåligt.png", size_hint=(1,1))
#PAYED_IMAGE_NEUTRAL = Image(source = PATH + "Grafik\\Bakgrundsbild_resultat_mellan.png", size_hint=(1,1))
#PAYED_IMAGE_GOOD = Image(source = PATH + "Grafik\\Bakgrundsbild_resultat_bra.png", size_hint=(1,1))
PAYED_IMAGE_BAD = PATH + "Grafik/Bakgrundsbild_resultat_dåligt.png"
PAYED_IMAGE_NEUTRAL = PATH + "Grafik/Bakgrundsbild_resultat_mellan.png"
PAYED_IMAGE_GOOD = PATH + "Grafik/Bakgrundsbild_resultat_bra.png"


DEFAULT_COLOR = (0, 0, 0, 1)
ENVIROMENTAL_SCORE = 0

CART_LIMIT = [1540, 160, 1770, 240, 1320, 560, 1700, 700]
 
#Window.resize = False
Window.size = (1920, 1080)

Window.borderless = True
Window.fullscreen = "auto"
Window.show_cursor = False
#Window.clear_widgets()
class MyLabel(Label):
   def on_size(self, *args):
      self.text_size = self.size

class ui(FloatLayout):
    def __init__(self, **kwargs):
        super(ui, self).__init__(**kwargs)
        self.background_image = Image(source=BG_IMAGE, size_hint=(1,1))
        self.add_widget(self.background_image)
        
        self.main_layout = BoxLayout(size_hint=(1,1), orientation="horizontal")
        self.add_widget(self.main_layout)
        self.left_side_box = BoxLayout(orientation="vertical", size_hint=(1,1))
        self.main_layout.add_widget(self.left_side_box)
        self.cart = FloatLayout()
        self.main_layout.add_widget(self.cart)
        
        self.indicator_box = BoxLayout(size_hint=(555/(1920/2), 183/1080), orientation="vertical")
        self.left_side_box.add_widget(self.indicator_box)
        self.goods_box = BoxLayout(size_hint=(1, 1), orientation="vertical")
        self.left_side_box.add_widget(self.goods_box)
        self.goods_framing_box = BoxLayout(orientation="horizontal")
        self.goods_box.add_widget(self.goods_framing_box)
        self.goods_left_framing = Label(size_hint=(300/(1920/2), 1))
        self.goods_framing_box.add_widget(self.goods_left_framing)
        self.goods = BoxLayout(orientation="vertical", padding=(10,200))
        self.goods_framing_box.add_widget(self.goods)
        #self.goods.add_widget(Label(size_hint=(1,400/1080)))
        
        self.enviromental_affect_image = Image(source=ENVIROMENTAL + "Knappar - släckta.png", pos_hint={'right': 1, 'bottom': 1})
        self.indicator_box.add_widget(self.enviromental_affect_image)
        
        self.s0 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s1 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s2 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s3 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s4 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s5 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s6 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s7 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s8 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s9 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s10 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s11 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s12 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        self.s13 = MyLabel(text="", color=DEFAULT_COLOR, font_size="25sp", halign="left", valign="middle")
        
        self.goods.add_widget(self.s0)
        self.goods.add_widget(self.s1)
        self.goods.add_widget(self.s2)
        self.goods.add_widget(self.s3)
        self.goods.add_widget(self.s4)
        self.goods.add_widget(self.s5)
        self.goods.add_widget(self.s6)
        self.goods.add_widget(self.s7)
        self.goods.add_widget(self.s8)
        self.goods.add_widget(self.s9)
        self.goods.add_widget(self.s10)
        self.goods.add_widget(self.s11)
        self.goods.add_widget(self.s12)
        self.goods.add_widget(self.s13)
        self.s_list = [self.s0,self.s1,self.s2,self.s3,self.s4,self.s5,self.s6,self.s7,self.s8,self.s9,self.s10,self.s11,self.s12,self.s13]
        
        self.start_info = Image(source=f"{PATH}Grafik/Bakgrundsbild_start.png")
        self.add_widget(self.start_info)
class RotatedImage(Image):
    angle = NumericProperty()
        
class hello(App):
    def build(self):
        self.items_in_cart = []
        self.already_scanned = []
        
        Builder.load_string('''
<RotatedImage>:
    canvas.before:
        PushMatrix
        Rotate:
            angle: root.angle
            axis: 0, 0, 1
            origin: root.center
    canvas.after:
        PopMatrix
''')
        self.barcode = Barcode_scanner()
        Clock.schedule_interval(self.mainloop, 1 / 30.)
        self.new_gui = ui()
        return self.new_gui
    
    def show_start(self):
        try:
            self.new_gui.start_info
            self.new_gui.remove_widget(self.new_gui.start_info)
        except:
            pass
        self.new_gui.add_widget(self.new_gui.start_info)
        
    def show_info(self, info, arg=3):
        try:
            self.information
            self.new_gui.remove_widget(self.information)
        except:
            pass
        self.information = Image(source=info)
        self.new_gui.add_widget(self.information)
        Clock.schedule_once(self.hide_info, arg)
        if arg != 3:
            self.score = Label(text=str(ENVIROMENTAL_SCORE), color=DEFAULT_COLOR, font_size="45sp", size_hint=(None, None), size=(50,50), pos_hint={"center_x": 600/1920, "center_y": (1080-650)/1080})
            #self.score = Label(text=str(ENVIROMENTAL_SCORE), pos=(10,25), color=DEFAULT_COLOR, font_size="45sp", halign="left", valign="middle", size=(50,50))
            self.new_gui.add_widget(self.score)
            Clock.schedule_once(self.freeze, 1)
        
    def freeze(self,dt):
        time.sleep(18)
        self.new_gui.remove_widget(self.score)
        self.reset()
        self.show_start()
        
    def show_product(self, prod):
        try:
            self.product
            self.new_gui.remove_widget(self.product)
        except:
            pass
        self.product = RotatedImage(source=prod, size_hint=(None, None), pos_hint={'center_x': 0.5, 'center_y': 0.5}, size=((500,500)), nocache=True)
        self.new_gui.add_widget(self.product)
        Clock.schedule_once(self.hide_product, 1)
        
    def hide_product(self,dt):
        self.new_gui.remove_widget(self.product)
        gc.collect()
        pass
    
    def hide_info(self,dt):
        self.new_gui.remove_widget(self.information)
        gc.collect()
    
    def add_image_to_cart(self,image, dt):
        self.new_gui.cart.add_widget(image)
        
    def reset(self):
        for i in range(len(self.items_in_cart)):
            self.new_gui.cart.remove_widget(self.items_in_cart[i])
            self.new_gui.s_list[i].text = ""
        self.items_in_cart = []
        self.already_scanned = []
        global ENVIROMENTAL_SCORE
        ENVIROMENTAL_SCORE = 0
        
    def mainloop(self, dt):
        self.shop_more = 0
        global ENVIROMENTAL_SCORE
        if self.barcode.result:
            match = ""
            name = ""
            rows = []
            fields = []
            add_price = 0
            #C:\Users\admin\Documents\Python\Exploratoriet\exploratoriet\Food\ui\Taco.csv
            with open(PATH + "Food/ui/" + "Taco.csv", 'r', encoding="utf8") as csvfile:
                # creating a csv reader object
                csvreader = csv.reader(csvfile, delimiter=";")
                # extracting field names through first row
                fields = next(csvreader)
                # extracting each data row one by one
                for row in csvreader:
                    rows.append(row)
                for row in rows:
                    if str(self.barcode.result) == START:
                        self.show_start()
                        self.shop_more = 1
                        self.reset()
                        break
                    elif str(self.barcode.result) == SHOW_RESULT:
                        if len(self.items_in_cart) > 6:
                            if ENVIROMENTAL_SCORE > 17:
                                self.show_info(PAYED_IMAGE_GOOD, 7)
                            elif ENVIROMENTAL_SCORE > -1:
                                self.show_info(PAYED_IMAGE_NEUTRAL, 7)
                            elif ENVIROMENTAL_SCORE < 0:
                                self.show_info(PAYED_IMAGE_BAD, 7)
                            self.shop_more = 1
                            self.reset()
                            break
                        else:
                            self.show_info(PATH+"Grafik/meddelande_forsätt_handla.png")
                            self.shop_more = 1
                            break                        
                    
                    if str(self.barcode.result) == str(row[1]).replace(" ", ""):
                        print(str(row[0]) + ".png")
                        if glob.glob(PATH + "Varor_kassa/" + str(row[0]) + ".png"):
                            match = str(row[0]) + ".png"
                            name = str(row[0])
                            self.current_score = int(row[2])        
                        try:
                            self.new_gui.remove_widget(self.new_gui.start_info)
                        except:
                            pass
                        break
                if match:                                                             #CART_LIMIT = [1540, 160, 1770, 240, 1320, 560, 1700, 700]
                    if match not in self.already_scanned:
                        ENVIROMENTAL_SCORE = ENVIROMENTAL_SCORE + int(row[2])
                        x = random.randint(900,1400)
                        y = random.randint(100,650)
                        item = RotatedImage(source=f"{PATH}Varor_kassa/{match}", angle=random.randint(0,360), pos=(x,y), size_hint=(None, None), size=((350,350)), nocache=True)
                        Clock.schedule_once(lambda dt:self.new_gui.cart.add_widget(item), 1)
                        if len(self.items_in_cart) > 30:
                            self.new_gui.cart.remove_widget(self.items_in_cart[0])
                            gc.collect()
                            self.items_in_cart = self.items_in_cart[1:]
                        self.items_in_cart.append(item)
                        self.show_product(f"{PATH}/Varor_kassa/{match}")
                        self.already_scanned.append(match)
                        if self.current_score < 0:
                            self.new_gui.enviromental_affect_image.source = ENVIROMENTAL + ENVIROMENTAL_NEGATIVE
                        elif self.current_score == 0:
                            self.new_gui.enviromental_affect_image.source = ENVIROMENTAL + ENVIROMENTAL_NEUTRAL
                        elif self.current_score > 1:
                            self.new_gui.enviromental_affect_image.source = ENVIROMENTAL + ENVIROMENTAL_POSITIVE
                        print(ENVIROMENTAL_SCORE)
                    
                else:
                    if self.shop_more == 1:
                        pass
                    else:
                        self.show_product(f"{PATH}/Food/ui/Ingen_vara/ingen_vara.png")

                
                
            if name:
                for i in range(14):
                    label = self.new_gui.s_list[i]
                    if label.text == name:
                        break
                else:
                    for i in range(14):
                        label = self.new_gui.s_list[i]
                        if label.text == "":
                            label.text = name
                            break
                for i in range(14):
                    if self.new_gui.s_list[i].text != "":
                        pass
                    else:
                        break
                else:
                    self.show_info(PATH + "Grafik/meddelande_du_har_handlat_max.png")
                               
                
            self.barcode.result = ""
hello().run()