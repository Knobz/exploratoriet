from kivy.core.window import Window

class Barcode_scanner():
    def __init__(self):
        self.scanned_code = []
        self.temp = []
        self.result = ""
        self.key = Window.request_keyboard(self.key_press, None)
        self.key.bind(on_key_down=self.key_press)
    
    def key_press(self, keyboard, keycode, text, modifiers):
        self.temp.append(keycode[1])
        if self.temp[-1] == "enter":
            self.scanned_code = self.temp
            self.temp = []
            self.result = ""
            for i in self.scanned_code[:-1]:
                self.result = self.result + i