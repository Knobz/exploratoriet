import kivy
from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import NumericProperty
from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.uix.image import Image
from kivy.uix.button import Button
from kivy.core.window import Window
from barcode_scanner import Barcode_scanner
from kivy.clock import Clock
from kivy.lang import Builder
import random
kivy.require("2.0.0")
import os
import csv
import serial
import gc
import time

RESET = 0
#PATH = "/home/pi/Documents/teknikens_hus/Food/ui/"
PATH = "C:\\Users\\admin\\Documents\\Python\\Exploratoriet\\"
#BG_IMAGE = PATH + "Grafik/Grafik-kassa-bgrd.png"
BG_IMAGE = PATH + "Grafik\\Bakgrundsbild_handla.png"
#ENVIROMENTAL = PATH + "Grafik/"
ENVIROMENTAL = PATH + "Grafik\\"
ENVIROMENTAL_NEGATIVE = "Knappa-ajdå val.png"
ENVIROMENTAL_NEUTRAL = "Knappa-nja val.png"
ENVIROMENTAL_POSITIVE = "Knappa-snyggt val.png"
PAYED_IMAGE_BAD = Image(source = PATH + "Grafik\\Bakgrundsbild_resultat_dåligt.png", size_hint=(1,1))
PAYED_IMAGE_NEUTRAL = Image(source = PATH + "Grafik\\Bakgrundsbild_resultat_mellan.png", size_hint=(1,1))
PAYED_IMAGE_GOOD = Image(source = PATH + "Grafik\\Bakgrundsbild_resultat_bra.png", size_hint=(1,1))

DEFAULT_COLOR = (86/255, 86/255, 84/255, 1)
ENVIROMENTAL_SCORE = 0

CART_LIMIT = [1540, 160, 1770, 240, 1320, 560, 1700, 700]
 
#Window.resize = False
Window.size = (1920, 1080)

Window.borderless = True
Window.fullscreen = "auto"
#Window.show_cursor = False
#Window.clear_widgets()

class ui(FloatLayout):
    def __init__(self, **kwargs):
        super(ui, self).__init__(**kwargs)
        self.background_image = Image(source=BG_IMAGE, size_hint=(1,1))
        self.add_widget(self.background_image)
        
        self.main_layout = BoxLayout(size_hint=(0.5,1), orientation="vertical")
        self.add_widget(self.main_layout)
        self.right_side_box = BoxLayout(orientation="vertical", size_hint=(0.5,1), pos_hint={"right":1})
        self.add_widget(self.right_side_box)
        
        button_box_top = Button(size_hint=(1, 14/1080))
        self.main_layout.add_widget(button_box_top)
        
        
        #self.buttons_box = BoxLayout(orientation="vertical", size_hint=((28+555)/1920, (14+183)/1080))
        self.buttons_box = Button()
        self.main_layout.add_widget(self.buttons_box)
        """
        button_box_left = Button(size_hint=(28/1920, 1), pos_hint={"left":1})
        self.buttons_box.add_widget(button_box_left)
        self.enviromental_affect_image = Image(source=ENVIROMENTAL + "Knappar - släckta.png", size_hint=((555*2)/1920, 183/1080), pos_hint={'top': 1, "left": 1})
        #self.main_layout.add_widget(self.enviromental_affect_image)
        self.buttons_box.add_widget(Button(size_hint=(555/1920,183/1080)))"""
        
        
        
        
        self.nothing = Button(size_hint=(1,100/1080), background_color =(1, 0, 0, 1))
        self.main_layout.add_widget(self.nothing)
        
        self.main_boxlayout = BoxLayout(orientation="horizontal", pos_hint={"left":1}, size_hint=(1, (1080-480)/1080))
        self.main_layout.add_widget(self.main_boxlayout)
        
        self.goods = BoxLayout(orientation="vertical",size_hint=(0.5,1))
        self.price = BoxLayout(orientation="vertical",size_hint=(0.3,1))
        
        self.main_boxlayout.add_widget(Button(size_hint=(0.1,1)))
        self.main_boxlayout.add_widget(self.goods)
        self.main_boxlayout.add_widget(self.price)
        self.main_boxlayout.add_widget(Button(size_hint=(0.1,1)))        
        
        self.s0 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.s1 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.s2 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.s3 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.s4 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.s5 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.s6 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.s7 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.s8 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.s9 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        
        self.goods.add_widget(self.s0)
        self.goods.add_widget(self.s1)
        self.goods.add_widget(self.s2)
        self.goods.add_widget(self.s3)
        self.goods.add_widget(self.s4)
        self.goods.add_widget(self.s5)
        self.goods.add_widget(self.s6)
        self.goods.add_widget(self.s7)
        self.goods.add_widget(self.s8)
        self.goods.add_widget(self.s9)
        
        
        self.t0 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.t1 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.t2 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.t3 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.t4 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.t5 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.t6 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.t7 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.t8 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        self.t9 = Button(text="", color=DEFAULT_COLOR, font_size="35sp")
        
        self.price.add_widget(self.t0)
        self.price.add_widget(self.t1)
        self.price.add_widget(self.t2)
        self.price.add_widget(self.t3)
        self.price.add_widget(self.t4)
        self.price.add_widget(self.t5)
        self.price.add_widget(self.t6)
        self.price.add_widget(self.t7)
        self.price.add_widget(self.t8)
        self.price.add_widget(self.t9)
        
        self.s_list = [self.s0,self.s1,self.s2,self.s3,self.s4,self.s5,self.s6,self.s7,self.s8,self.s9]
        self.t_list = [self.t0,self.t1,self.t2,self.t3,self.t4,self.t5,self.t6,self.t7,self.t8,self.t9]
        
        self.price_layout = BoxLayout(orientation="horizontal", size_hint=(1, 197/1080))
        self.main_layout.add_widget(self.price_layout)
        
        self.push_label = Button(size_hint = (0.25, 1))
        self.price_layout.add_widget(self.push_label)
        self.current_price = Button(text=f"{1} Kr", size_hint=(0.75, 1), font_size="45sp", color=DEFAULT_COLOR)
        self.price_layout.add_widget(self.current_price)
        
        self.right_side_box.add_widget(Button(size_hint=(1,0.1), pos_hint={"top":1}))
        self.cart_framing_box = BoxLayout(orientation="horizontal", size_hint=(1,0.7))
        self.right_side_box.add_widget(self.cart_framing_box)
        #########################################
        self.cart_framing_box.add_widget(Button(size_hint=(0.2,1)))
        self.cart = FloatLayout(size_hint=(0.7, 1))
        self.cart_framing_box.add_widget(self.cart)
        self.cart_framing_box.add_widget(Button(size_hint=(0.1,1)))
        ###################################################
        self.fund_image = Image(source=PATH+"Grafik/current_funds.png", pos_hint={"right":1})
        
        self.fund_box = FloatLayout(size_hint=(1,0.2), pos_hint={"right":1})
        self.right_side_box.add_widget(self.fund_box)
        self.fund_box.add_widget(self.fund_image)
        self.fund_label = Button(text=f"{1} Kr", size_hint=(0.6,1), pos_hint={"right":1}, font_size="45sp", color=DEFAULT_COLOR)
        self.fund_box.add_widget(self.fund_label)
        #self.right_side_box.add_widget(Button(size_hint=(1,0.2)))
        

class RotatedImage(Image):
    angle = NumericProperty()
        
class hello(App):
    def build(self):
        self.items_in_cart = []
        Builder.load_string('''
<RotatedImage>:
    canvas.before:
        PushMatrix
        Rotate:
            angle: root.angle
            axis: 0, 0, 1
            origin: root.center
    canvas.after:
        PopMatrix
''')
        self.barcode = Barcode_scanner()
        Clock.schedule_interval(self.mainloop, 1 / 30.)
        self.new_gui = ui()
        return self.new_gui
    
                
    def remove_payed(self, dt):
        #self.new_gui.remove_widget()
        pass
        
    def mainloop(self, dt):
        if self.barcode.result:
            match = ""
            name = ""
            rows = []
            fields = []
            add_price = 0
            with open(PATH + "Kassan.csv", 'r', encoding="utf8") as csvfile:
                # creating a csv reader object
                csvreader = csv.reader(csvfile, delimiter=";")
                
                # extracting field names through first row
                fields = next(csvreader)
            
                # extracting each data row one by one
                for row in csvreader:
                    rows.append(row)
                for row in rows:
                    if str(self.barcode.result) == str(row[1]).replace(" ", ""):
                        match = str(row[0]) + ".png"
                        name = str(row[0])
                        add_price = int(row[2])
                        global ENVIROMENTAL_SCORE
                        ENVIROMENTAL_SCORE = ENVIROMENTAL_SCORE + int(row[3])
                        break
                if match:                                                               #CART_LIMIT = [1540, 160, 1770, 240, 1320, 560, 1700, 700]
                    x = random.randint(1400,1600)
                    y = random.randint(300,700)
                    item = RotatedImage(source=f"{PATH}\\Food\\ui\\Varor_kassa{match}", angle=random.randint(0,360), pos=(x,y), size_hint=(None, None), size=((250,250)), nocache=True)
                    if len(self.items_in_cart) > 30:
                        self.new_gui.cart.remove_widget(self.items_in_cart[0])
                        gc.collect()
                        self.items_in_cart = self.items_in_cart[1:]
                    self.items_in_cart.append(item)
                    self.new_gui.cart.add_widget(item)
                else:
                    pass
                
            if name:
                for i in range(10):
                    label = self.new_gui.s_list[i]
                    if label.text == name:
                        self.new_gui.t_list[i].text = str(int(self.new_gui.t_list[i].text[:-3]) + int(add_price)) + " Kr"
                        self.new_gui.t_list[i].text
                        break
                else:
                    for i in range(10):
                        label = self.new_gui.s_list[i]
                        if label.text == "":
                            label.text = name
                            self.new_gui.t_list[i].text = f"{add_price} Kr"
                            break
                self.new_gui.current_price.text = str(int(self.new_gui.current_price.text[:-3]) + int(add_price)) + " Kr"
                
                if ENVIROMENTAL_SCORE > 0:
                    self.new_gui.enviromental_affect_image.source = ENVIROMENTAL + ENVIROMENTAL_NEGATIVE
                elif ENVIROMENTAL_SCORE == 0:
                    self.new_gui.enviromental_affect_image.source = ENVIROMENTAL + ENVIROMENTAL_NEUTRAL
                elif ENVIROMENTAL_SCORE < 0:
                    self.new_gui.enviromental_affect_image.source = ENVIROMENTAL + ENVIROMENTAL_POSITIVE
            self.barcode.result = ""
hello().run()